/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 20:59:45                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.hybrisanalyticsaddon.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedHybrisanalyticsaddonConstants
{
	public static final String EXTENSIONNAME = "hybrisanalyticsaddon";
	public static class TC
	{
		public static final String HYBRISANALYTICSTAGSCRIPTCOMPONENT = "HybrisAnalyticsTagScriptComponent".intern();
	}
	
	protected GeneratedHybrisanalyticsaddonConstants()
	{
		// private constructor
	}
	
	
}
