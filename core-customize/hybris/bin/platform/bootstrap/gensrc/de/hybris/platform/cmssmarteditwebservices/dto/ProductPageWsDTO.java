/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:17
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmssmarteditwebservices.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @deprecated since 1811, no longer needed.
 */
@ApiModel(value="ProductPageWsDTO")
@Deprecated(forRemoval = true)
public  class ProductPageWsDTO extends AbstractPageWsDTO 
{

 
	
	public ProductPageWsDTO()
	{
		// default constructor
	}
	


}
