/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:15
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.ruleengineservices.compiler;

import de.hybris.platform.ruleengineservices.compiler.AbstractRuleIrAttributeCondition;

public  class RuleIrAttributeCondition extends AbstractRuleIrAttributeCondition 
{

 

	/** <i>Generated property</i> for <code>RuleIrAttributeCondition.value</code> property defined at extension <code>ruleengineservices</code>. */
		
	private Object value;
	
	public RuleIrAttributeCondition()
	{
		// default constructor
	}
	
	public void setValue(final Object value)
	{
		this.value = value;
	}

	public Object getValue() 
	{
		return value;
	}
	


}
