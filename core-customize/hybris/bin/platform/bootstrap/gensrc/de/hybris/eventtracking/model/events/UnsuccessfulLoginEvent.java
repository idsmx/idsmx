/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:08
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.eventtracking.model.events;

import java.io.Serializable;

import de.hybris.eventtracking.model.events.AbstractTrackingEvent;

public  class UnsuccessfulLoginEvent extends AbstractTrackingEvent {

	
	public UnsuccessfulLoginEvent()
	{
		super();
	}

	public UnsuccessfulLoginEvent(final Serializable source)
	{
		super(source);
	}
	


}
