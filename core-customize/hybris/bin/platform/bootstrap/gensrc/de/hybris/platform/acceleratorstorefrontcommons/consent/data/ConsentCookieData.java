/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:05
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.acceleratorstorefrontcommons.consent.data;

import de.hybris.platform.commercefacades.consent.data.AnonymousConsentData;

/**
 * @deprecated Since 1905. Use {@link AnonymousConsentData} instead
 */
@Deprecated(forRemoval = true)
public  class ConsentCookieData extends AnonymousConsentData 
{

 
	
	public ConsentCookieData()
	{
		// default constructor
	}
	


}
