/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:05
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.ruleengineservices.compiler;

import de.hybris.platform.ruleengineservices.compiler.RuleIrConditionWithChildren;
import de.hybris.platform.ruleengineservices.compiler.RuleIrLocalVariablesContainer;

public  class RuleIrExistsCondition extends RuleIrConditionWithChildren 
{

 

	/** <i>Generated property</i> for <code>RuleIrExistsCondition.variablesContainer</code> property defined at extension <code>ruleengineservices</code>. */
		
	private RuleIrLocalVariablesContainer variablesContainer;
	
	public RuleIrExistsCondition()
	{
		// default constructor
	}
	
	public void setVariablesContainer(final RuleIrLocalVariablesContainer variablesContainer)
	{
		this.variablesContainer = variablesContainer;
	}

	public RuleIrLocalVariablesContainer getVariablesContainer() 
	{
		return variablesContainer;
	}
	


}
