/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:17
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.ruleengineservices.rao;

import java.io.Serializable;

public  class CustomerSupportRAO  implements Serializable 
{

 	/** Default serialVersionUID value. */
 
 	private static final long serialVersionUID = 1L;

	/** <i>Generated property</i> for <code>CustomerSupportRAO.customerSupportAgentActive</code> property defined at extension <code>ruleengineservices</code>. */
		
	private Boolean customerSupportAgentActive;

	/** <i>Generated property</i> for <code>CustomerSupportRAO.customerEmulationActive</code> property defined at extension <code>ruleengineservices</code>. */
		
	private Boolean customerEmulationActive;
	
	public CustomerSupportRAO()
	{
		// default constructor
	}
	
	public void setCustomerSupportAgentActive(final Boolean customerSupportAgentActive)
	{
		this.customerSupportAgentActive = customerSupportAgentActive;
	}

	public Boolean getCustomerSupportAgentActive() 
	{
		return customerSupportAgentActive;
	}
	
	public void setCustomerEmulationActive(final Boolean customerEmulationActive)
	{
		this.customerEmulationActive = customerEmulationActive;
	}

	public Boolean getCustomerEmulationActive() 
	{
		return customerEmulationActive;
	}
	

	@Override
	public boolean equals(final Object o)
	{
	
		if (o == null) return false;
		if (o == this) return true;

		try
		{
			final CustomerSupportRAO other = (CustomerSupportRAO) o;
			return new org.apache.commons.lang.builder.EqualsBuilder()
			.append(getCustomerSupportAgentActive(), other.getCustomerSupportAgentActive()) 
			.append(getCustomerEmulationActive(), other.getCustomerEmulationActive()) 
			.isEquals();
		} 
		catch (ClassCastException c)
		{
			return false;
		}
	}
	
	@Override
	public int hashCode()
	{
		return new org.apache.commons.lang.builder.HashCodeBuilder()
		.append(getCustomerSupportAgentActive()) 
		.append(getCustomerEmulationActive()) 
		.toHashCode();
	}

}
