/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:11
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmsfacades.data;

import de.hybris.platform.cmsfacades.data.SyncRequestData;

/**
 * @deprecated since 1811, no longer needed
 */
@Deprecated(forRemoval = true)
public  class SyncJobRequestData extends SyncRequestData 
{

 
	
	public SyncJobRequestData()
	{
		// default constructor
	}
	


}
