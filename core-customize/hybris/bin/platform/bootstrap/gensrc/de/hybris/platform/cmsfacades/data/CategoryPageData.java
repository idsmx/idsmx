/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:17
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmsfacades.data;

/**
 * @deprecated Deprecated since 6.6
 */
@Deprecated(forRemoval = true)
public  class CategoryPageData extends AbstractPageData 
{

 
	
	public CategoryPageData()
	{
		// default constructor
	}
	


}
