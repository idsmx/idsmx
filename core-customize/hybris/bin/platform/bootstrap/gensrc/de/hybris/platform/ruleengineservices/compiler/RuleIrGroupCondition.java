/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:12
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.ruleengineservices.compiler;

import de.hybris.platform.ruleengineservices.compiler.RuleIrConditionWithChildren;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupOperator;

public  class RuleIrGroupCondition extends RuleIrConditionWithChildren 
{

 

	/** <i>Generated property</i> for <code>RuleIrGroupCondition.operator</code> property defined at extension <code>ruleengineservices</code>. */
		
	private RuleIrGroupOperator operator;
	
	public RuleIrGroupCondition()
	{
		// default constructor
	}
	
	public void setOperator(final RuleIrGroupOperator operator)
	{
		this.operator = operator;
	}

	public RuleIrGroupOperator getOperator() 
	{
		return operator;
	}
	


}
