/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:13
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.personalizationservices.data;

import de.hybris.platform.personalizationservices.data.BaseSegmentData;
import java.math.BigDecimal;

public  class UserToSegmentData extends BaseSegmentData 
{

 

	/** <i>Generated property</i> for <code>UserToSegmentData.affinity</code> property defined at extension <code>personalizationservices</code>. */
		
	private BigDecimal affinity;
	
	public UserToSegmentData()
	{
		// default constructor
	}
	
	public void setAffinity(final BigDecimal affinity)
	{
		this.affinity = affinity;
	}

	public BigDecimal getAffinity() 
	{
		return affinity;
	}
	


}
