/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:18
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercewebservicescommons.dto.product;

public enum ImageWsDTOType
{

	/** <i>Generated enum value</i> for <code>ImageWsDTOType.PRIMARY</code> value defined at extension <code>commercewebservicescommons</code>. */
	PRIMARY , 
	/** <i>Generated enum value</i> for <code>ImageWsDTOType.GALLERY</code> value defined at extension <code>commercewebservicescommons</code>. */
	GALLERY  

}
