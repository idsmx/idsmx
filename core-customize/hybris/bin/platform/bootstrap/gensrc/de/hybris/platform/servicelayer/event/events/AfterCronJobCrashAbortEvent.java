/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:16
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.servicelayer.event.events;

import java.io.Serializable;

import de.hybris.platform.servicelayer.event.events.AbstractCronJobEvent;

public  class AfterCronJobCrashAbortEvent extends AbstractCronJobEvent {

	
	public AfterCronJobCrashAbortEvent()
	{
		super();
	}

	public AfterCronJobCrashAbortEvent(final Serializable source)
	{
		super(source);
	}
	


}
