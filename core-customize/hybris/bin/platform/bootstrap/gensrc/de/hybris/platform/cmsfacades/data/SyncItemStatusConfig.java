/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:09
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmsfacades.data;

import java.io.Serializable;

public  class SyncItemStatusConfig  implements Serializable 
{

 	/** Default serialVersionUID value. */
 
 	private static final long serialVersionUID = 1L;

	/** <i>Generated property</i> for <code>SyncItemStatusConfig.maxDepth</code> property defined at extension <code>cmsfacades</code>. */
		
	private Integer maxDepth;
	
	public SyncItemStatusConfig()
	{
		// default constructor
	}
	
	public void setMaxDepth(final Integer maxDepth)
	{
		this.maxDepth = maxDepth;
	}

	public Integer getMaxDepth() 
	{
		return maxDepth;
	}
	


}
