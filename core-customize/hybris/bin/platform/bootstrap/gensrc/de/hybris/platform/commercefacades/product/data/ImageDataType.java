/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:18
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercefacades.product.data;

public enum ImageDataType
{

	/** <i>Generated enum value</i> for <code>ImageDataType.PRIMARY</code> value defined at extension <code>commercefacades</code>. */
	PRIMARY , 
	/** <i>Generated enum value</i> for <code>ImageDataType.GALLERY</code> value defined at extension <code>commercefacades</code>. */
	GALLERY  

}
