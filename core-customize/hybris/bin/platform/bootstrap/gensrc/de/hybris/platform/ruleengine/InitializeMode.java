/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:13
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.ruleengine;

public enum InitializeMode
{

	/** <i>Generated enum value</i> for <code>InitializeMode.RESTORE</code> value defined at extension <code>ruleengine</code>. */
	RESTORE , 
	/** <i>Generated enum value</i> for <code>InitializeMode.NEW</code> value defined at extension <code>ruleengine</code>. */
	NEW  

}
