/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:15
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.addonsupport.impex;

public enum AddonConfigDataImportType
{

	/** <i>Generated enum value</i> for <code>AddonConfigDataImportType.CONTENT</code> value defined at extension <code>addonsupport</code>. */
	CONTENT , 
	/** <i>Generated enum value</i> for <code>AddonConfigDataImportType.PRODUCT</code> value defined at extension <code>addonsupport</code>. */
	PRODUCT , 
	/** <i>Generated enum value</i> for <code>AddonConfigDataImportType.SOLR</code> value defined at extension <code>addonsupport</code>. */
	SOLR , 
	/** <i>Generated enum value</i> for <code>AddonConfigDataImportType.STORE</code> value defined at extension <code>addonsupport</code>. */
	STORE  

}
