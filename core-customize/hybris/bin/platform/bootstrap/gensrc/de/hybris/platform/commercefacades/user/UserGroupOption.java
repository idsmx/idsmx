/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:15
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercefacades.user;

public enum UserGroupOption
{

	/** <i>Generated enum value</i> for <code>UserGroupOption.BASIC</code> value defined at extension <code>commercefacades</code>. */
	BASIC , 
	/** <i>Generated enum value</i> for <code>UserGroupOption.MEMBERS</code> value defined at extension <code>commercefacades</code>. */
	MEMBERS , 
	/** <i>Generated enum value</i> for <code>UserGroupOption.SUBGROUPS</code> value defined at extension <code>commercefacades</code>. */
	SUBGROUPS  

}
