/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:10
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.acceleratorfacades.cart.action;

/**
 * Accelerator cart entry actions
 */
public enum CartEntryAction
{

	/** <i>Generated enum value</i> for <code>CartEntryAction.REMOVE</code> value defined at extension <code>acceleratorfacades</code>. */
	REMOVE  

}
