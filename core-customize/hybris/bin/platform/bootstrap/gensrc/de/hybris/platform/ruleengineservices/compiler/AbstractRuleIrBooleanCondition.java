/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:07
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.ruleengineservices.compiler;

import de.hybris.platform.ruleengineservices.compiler.RuleIrCondition;

public abstract  class AbstractRuleIrBooleanCondition extends RuleIrCondition 
{

 
	
	public AbstractRuleIrBooleanCondition()
	{
		// default constructor
	}
	


}
