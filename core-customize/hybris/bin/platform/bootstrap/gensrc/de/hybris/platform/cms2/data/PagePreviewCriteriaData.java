/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:11
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cms2.data;

import java.io.Serializable;

public  class PagePreviewCriteriaData  implements Serializable 
{

 	/** Default serialVersionUID value. */
 
 	private static final long serialVersionUID = 1L;

	/** <i>Generated property</i> for <code>PagePreviewCriteriaData.versionUid</code> property defined at extension <code>cms2</code>. */
		
	private String versionUid;
	
	public PagePreviewCriteriaData()
	{
		// default constructor
	}
	
	public void setVersionUid(final String versionUid)
	{
		this.versionUid = versionUid;
	}

	public String getVersionUid() 
	{
		return versionUid;
	}
	


}
