/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:15
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.solrfacetsearch.config;

public enum OptimizeMode
{

	/** <i>Generated enum value</i> for <code>OptimizeMode.NEVER</code> value defined at extension <code>solrfacetsearch</code>. */
	NEVER , 
	/** <i>Generated enum value</i> for <code>OptimizeMode.AFTER_INDEX</code> value defined at extension <code>solrfacetsearch</code>. */
	AFTER_INDEX , 
	/** <i>Generated enum value</i> for <code>OptimizeMode.AFTER_FULL_INDEX</code> value defined at extension <code>solrfacetsearch</code>. */
	AFTER_FULL_INDEX  

}
