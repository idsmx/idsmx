/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 21:00:10
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercefacades.product;

public enum ProductVariantOption
{

	/** <i>Generated enum value</i> for <code>ProductVariantOption.URL</code> value defined at extension <code>commercefacades</code>. */
	URL , 
	/** <i>Generated enum value</i> for <code>ProductVariantOption.MEDIA</code> value defined at extension <code>commercefacades</code>. */
	MEDIA , 
	/** <i>Generated enum value</i> for <code>ProductVariantOption.PRICE</code> value defined at extension <code>commercefacades</code>. */
	PRICE , 
	/** <i>Generated enum value</i> for <code>ProductVariantOption.STOCK</code> value defined at extension <code>commercefacades</code>. */
	STOCK  

}
